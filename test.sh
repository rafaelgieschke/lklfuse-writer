#!/bin/sh

qemu-img create a.img -f qcow2 1G
ls -la a.img

fuseqemu img a.img -f qcow2 -o=--cache=unsafe & p="$!"
while test ! -s img; do :; done

mkfs.ext4 img
ls -la a.img img

lklfuse -o type=ext4 img /mnt
while test "$(stat -c "%m" /mnt)" != "/mnt"; do echo 2; done

echo a > /mnt/aa
dd if="/lib/x86_64-linux-gnu/libc-2.27.so" of=/mnt/test2 bs=1M oflag=sync
ls -la /mnt


ls -la
fusermount -u -z /mnt
fusermount -u -z img
ls -la

wait "$p"

ls -la
sleep 10
ls -la

fuseqemu img a.img -f qcow2 -o=--cache=unsafe & p="$!"
while ! test -s img; do :; done
lklfuse -o type=ext4 img /mnt
ls -la /mnt
fusermount -uz /mnt
fusermount -uz img
wait "$p"
echo DONE
