from registry.gitlab.com/rafaelgieschke/fuseqemu
copy --from=registry.gitlab.com/emulation-as-a-service/lklfuse /opt/lkl /opt/fuseqemu
copy test.sh .
cmd ./test.sh
